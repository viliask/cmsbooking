# System „BookingCMS” #

Cel projektu:

Stworzenie aplikacji ułatwiającej zarządzanie pokojami do wynajęcia.

Aplikacja powinna zawierać:

- funkcjonalność dodawania nowych stron z edytora

- zarządzanie zasobami wraz z widokiem kalendarza

- możliwość wynajmu pokoju z przesłaniem informacji na maila

- musi być bardzo prosta w użytkowaniu

TODO:

- system bookingowy

- WYSIWYG dla stron blogowych i zwykłych

- front

- ogólna funkcjonalność

Screeny:

![homepage](https://bitbucket.org/ziemniaki666/cmsbooking/raw/2880a37cee62705c63aa90e90a181619bd785372/public/readme/homepage.jpg)

![admin](https://bitbucket.org/ziemniaki666/cmsbooking/raw/2880a37cee62705c63aa90e90a181619bd785372/public/readme/admin.jpg)

![de](https://bitbucket.org/ziemniaki666/cmsbooking/raw/2880a37cee62705c63aa90e90a181619bd785372/public/readme/de.jpg)

![easybundle](https://bitbucket.org/ziemniaki666/cmsbooking/raw/2880a37cee62705c63aa90e90a181619bd785372/public/readme/easybundle.jpg)

![blog](https://bitbucket.org/ziemniaki666/cmsbooking/raw/2880a37cee62705c63aa90e90a181619bd785372/public/readme/blog.jpg)