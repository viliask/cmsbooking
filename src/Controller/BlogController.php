<?php

namespace App\Controller;

use App\Entity\Blog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use \Symfony\Component\HttpFoundation\Response;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{id}", name="blog", methods={"GET"})
     */
    public function show($id)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('App:Blog')->find($id);

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $this->render(
            'blog/show.html.twig',
            array(
                'blog' => $blog,
            )
        );
    }


    /**
     * //     * @param Blog $post
     * @Route("/blog", name="latest_posts", methods={"GET"})
     */
    public function showPosts()
    {
//        $form = $this->createForm(TextForm::class, new Text());
//
//        $form->add('body',  CKEditorType::Class);
//
//        $form = $this->createFormBuilder($post)
//            ->add('title', 'text')
//            ->add('content', 'ckeditor', array(
//                'transformers' => array(),
//            ))
//            ->getForm()
//        ;

        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('App:Blog')->findAll();

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $this->render(
            'blog/latest.html.twig',
            array(
                'blog' => $blog,
            )
        );
    }
}
